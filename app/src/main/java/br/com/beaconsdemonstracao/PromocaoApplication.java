package br.com.beaconsdemonstracao;

import android.app.Application;
import com.kontakt.sdk.android.common.KontaktSDK;

/**
 * Created by daniel on 28/08/17.
 */

public class PromocaoApplication extends Application {

    private static PromocaoApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        KontaktSDK.initialize(this);
    }

    public static PromocaoApplication getInstance() {
        return instance;
    }

    public static void setInstance(PromocaoApplication instance) {
        PromocaoApplication.instance = instance;
    }
}

package br.com.beaconsdemonstracao;

import android.util.Log;
import com.kontakt.sdk.android.ble.configuration.ActivityCheckConfiguration;
import com.kontakt.sdk.android.ble.configuration.ScanMode;
import com.kontakt.sdk.android.ble.configuration.ScanPeriod;
import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.device.EddystoneNamespace;
import com.kontakt.sdk.android.ble.exception.ScanError;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerFactory;
import com.kontakt.sdk.android.ble.manager.listeners.ScanStatusListener;
import com.kontakt.sdk.android.ble.manager.listeners.simple.SimpleEddystoneListener;
import com.kontakt.sdk.android.common.profile.IEddystoneDevice;
import com.kontakt.sdk.android.common.profile.IEddystoneNamespace;
import java.util.HashMap;
import java.util.Map;


public class BeaconSingleton{

    private static BeaconSingleton beaconSingleton = null;
    private ProximityManager proximityManager;
    private Map<String, IEddystoneDevice> devices;

    public BeaconSingleton(){
        //inicializar variaveis
        this.devices = new HashMap<>();

        //criar objeto proximityManager
        this.proximityManager = ProximityManagerFactory.create(PromocaoApplication.getInstance());

        this.proximityManager.configuration()
                .scanPeriod(ScanPeriod.RANGING)
                .scanMode(ScanMode.LOW_LATENCY)
                .monitoringEnabled(true)
                .monitoringSyncInterval(1)
                .activityCheckConfiguration(ActivityCheckConfiguration.MINIMAL);


        IEddystoneNamespace mjuR = new EddystoneNamespace.Builder()
                .identifier("mjur")
                .namespace("f7826da64fa24e988001")
                .instanceId("4f6554527661")
                .build();

        IEddystoneNamespace Si1U = new EddystoneNamespace.Builder()
                .identifier("Si1U")
                .namespace("f7826da64fa24e988002")
                .instanceId("6a36634a624d")
                .build();


        /*this.proximityManager.setSpaceListener(new EddystoneSpaceListener() {
            @Override
            public void onNamespaceEntered(IEddystoneNamespace namespace) {
                Log.i("BeaconSingleton", "onNamespaceEntered: " + namespace.getIdentifier());
            }

            @Override
            public void onNamespaceAbandoned(IEddystoneNamespace namespace) {
                Log.i("BeaconSingleton", "onNamespaceAbandoned: " + namespace.getIdentifier());
            }
        });*/

        //callback que vai encontrar os beacons
        this.proximityManager.setEddystoneListener(new SimpleEddystoneListener() {
            @Override
            public void onEddystoneDiscovered(IEddystoneDevice eddystone, IEddystoneNamespace namespace) {
                Log.i("BeaconSingleton", "Eddystone discovered: " + eddystone);
                devices.put(eddystone.getUniqueId(), eddystone);
            }
        });


        this.proximityManager.setScanStatusListener(new ScanStatusListener() {
            @Override
            public void onScanStart() {
                Log.i("BeaconSingleton", "Scanner iniciado");
            }

            @Override
            public void onScanStop() {
                Log.i("BeaconSingleton", "Scanner parado");
            }

            @Override
            public void onScanError(ScanError error) {
                Log.i("BeaconSingleton", "Erro no scanner");
            }

            @Override
            public void onMonitoringCycleStart() {
                Log.i("BeaconSingleton", "Monitoring cycle started");
            }

            @Override
            public void onMonitoringCycleStop() {
                Log.i("BeaconSingleton", "Monitoring cycle finished");
            }
        });

        this.proximityManager.connect(new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                Log.i("BeaconSingleton", "ProximityManager connected");
                proximityManager.startScanning();
            }
        });
    }

    public void connect(){
        this.proximityManager.connect(new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                Log.i("BeaconSingleton", "ProximityManager connected");
                proximityManager.startScanning();
            }
        });
    }

    public static BeaconSingleton getInstance(){
        if(beaconSingleton == null){
            Log.d("BeaconSingleton", "BeaconSingleton constructor");
            beaconSingleton = new BeaconSingleton();
            return beaconSingleton;
        }else{
            return beaconSingleton;
        }
    }

    public ProximityManager getProximityManager() {
        return proximityManager;
    }

    public void setProximityManager(ProximityManager proximityManager) {
        this.proximityManager = proximityManager;
    }

    public Map<String, IEddystoneDevice> getDevices() {
        return devices;
    }

    public void setDevices(HashMap<String, IEddystoneDevice> devices) {
        this.devices = devices;
    }

    @Override
    protected void finalize() throws Throwable {
        Log.d("BeaconSingleton", "Finalize called");
        beaconSingleton = getInstance();
    }


}

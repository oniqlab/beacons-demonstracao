package br.com.beaconsdemonstracao;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import com.kontakt.sdk.android.common.profile.IEddystoneDevice;
import java.util.ArrayList;

public class MainActivity extends Activity {

    private int permissionCheck;
    private static final String DISTANCIA_BEACON = "0.0";
    private Context context;
    public static boolean threadScanControl;
    public static boolean threadUiControl;
    private Thread threadScan;
    private Thread threadUi;
    private static boolean a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_main);
        this.context = this;
        this.a = true;
        this.threadScanControl = true;
        this.threadUiControl = true;
        permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if(permissionCheck == PackageManager.PERMISSION_GRANTED){
                BeaconSingleton.getInstance();
            }

            threadUi = new Thread() {
                @Override
                public void run() {
                    try {
                        while (threadScanControl) {
                            sleep(2000);
                            for (String key : BeaconSingleton.getInstance().getDevices().keySet()) {
                                IEddystoneDevice device = BeaconSingleton.getInstance().getDevices().get(key);
                                Log.i("BeaconSingleton", "KEY " + device.getDistance());

                                //se a distancia bater, startar a activity,
                                String distancia = (device.getDistance() + "");
                                String y = distancia.substring(0, 3);
                                if (y.equals(DISTANCIA_BEACON)) {
                                    if(a){
                                        a = false;
                                        Intent promo = new Intent(context, Promocao.class);
                                        context.startActivity(promo);
                                    }
                                }


                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            };

            threadScan = new Thread() {
                @Override
                public void run() {
                    try {
                        while(threadUiControl) {
                            sleep(2000);
                            if(BeaconSingleton.getInstance().getProximityManager().isConnected()){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        BeaconSingleton.getInstance().getProximityManager().restartScanning();
                                    }
                                });
                            }
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch(Exception e ){
                        e.printStackTrace();
                    }
                }
            };

            this.threadScan.start();
            this.threadUi.start();

    }

    @Override
    protected void onStart() {
        if(permissionCheck != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        this.threadScan.start();
        this.threadUi.start();
        super.onStart();
    }

    @Override
    protected void onStop() {
        if(permissionCheck == PackageManager.PERMISSION_GRANTED)
            BeaconSingleton.getInstance();

        BeaconSingleton.getInstance().getProximityManager().disconnect();
        threadScanControl = false;
        threadUiControl = false;
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if(permissionCheck == PackageManager.PERMISSION_GRANTED)
            BeaconSingleton.getInstance();

        threadScanControl = false;
        threadUiControl = false;
        BeaconSingleton.getInstance().getProximityManager().disconnect();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case 1: {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    BeaconSingleton.getInstance();
                }
            }
        }
    }


}
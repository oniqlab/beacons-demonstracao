package br.com.beaconsdemonstracao;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

public class Promocao extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.promo);
        MainActivity.threadUiControl = false;
        MainActivity.threadScanControl = false;
    }
}

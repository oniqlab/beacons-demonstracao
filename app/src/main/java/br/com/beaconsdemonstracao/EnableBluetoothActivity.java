package br.com.beaconsdemonstracao;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by daniel on 29/08/17.
 */

public class EnableBluetoothActivity extends Activity {

    BluetoothAdapter mBluetoothAdapter;
    Context context;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.enable_bluetooth);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter.isEnabled()) {
            Intent main = new Intent(this, MainActivity.class);
            startActivity(main);
        }
        context = this;

        final Button button = findViewById(R.id.enable_bluetooth);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBluetoothAdapter.enable();
                    Toast.makeText(context, "Bluetooth Ligado", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
            }
        });
    }
}
